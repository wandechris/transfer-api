package com.akinwande.revolut.tests.unit;

import com.akinwande.revolut.controller.TransferController;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;
import org.junit.Assert;
import org.junit.Test;


public class ValidationTest extends TestBase{

    @Inject
    TransferController transferController;

    @Test
    public void validateAmount()
    {
        try {
            TransferRequest transferRequest = new TransferRequest();
            transferController.validate(transferRequest);
        }catch (Exception e){
            Assert.assertEquals(TransferUtil.INVALID_AMOUNT,e.getMessage());
        }
    }

    @Test
    public void validateInvlaidAmount() {
        try {
            TransferRequest transferRequest = new TransferRequest();
            transferRequest.setAmount(0);
            transferRequest.setSourceAccountNumber("source");
            transferRequest.setDestinationAccountNumber("destination");
            transferController.validate(transferRequest);
        }catch (Exception e){
            Assert.assertEquals(TransferUtil.INVALID_AMOUNT,e.getMessage());
        }
    }

    @Test
    public void validateSourceAccountNumber() {
        try {
            TransferRequest transferRequest = new TransferRequest();
            transferRequest.setAmount(23);
            transferController.validate(transferRequest);
        }catch (Exception e){
            Assert.assertEquals(TransferUtil.INVALID_DESTINATION_ACCOUNT_NUMBER,e.getMessage());
        }
    }


    @Test
    public void validateDestinationAccountNumber() {
        try {
            TransferRequest transferRequest = new TransferRequest();
            transferRequest.setAmount(23);
            transferRequest.setDestinationAccountNumber("destination");
            transferController.validate(transferRequest);
        }catch (Exception e){
            Assert.assertEquals(TransferUtil.INVALID_SOURCE_ACCOUNT_NUMBER,e.getMessage());
        }
    }
}
