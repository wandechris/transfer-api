package com.akinwande.revolut.tests.unit;

import com.akinwande.revolut.controller.TransferController;
import com.akinwande.revolut.repository.AccountRepository;
import com.akinwande.revolut.repository.Impl.AccountRepositoryMapImpl;
import com.akinwande.revolut.repository.TransfersRepository;
import com.akinwande.revolut.repository.Impl.TransfersRepositoryMapImpl;
import com.akinwande.revolut.service.AccountService;
import com.akinwande.revolut.service.TransferService;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;

public class TestBase {
    protected Injector injector = Guice.createInjector(new AbstractModule() {
        @Override
        protected void configure() {
            bind(TransferController.class);
            bind(TransferService.class);
            bind(AccountService.class);
            bind(TransfersRepository.class).to(TransfersRepositoryMapImpl.class);
            bind(AccountRepository.class).to(AccountRepositoryMapImpl.class);
        }
    });

    @Before
    public void setup () {injector.injectMembers(this); }
}
