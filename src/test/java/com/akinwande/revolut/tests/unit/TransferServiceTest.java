package com.akinwande.revolut.tests.unit;

import com.akinwande.revolut.dao.Account;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.dao.TransferResponse;
import com.akinwande.revolut.service.AccountService;
import com.akinwande.revolut.service.TransferService;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TransferServiceTest extends TestBase{
    @Inject
    TransferService transferService;

    @Before
    public void initialize(){
        Account account = new Account();
        account.setAccountName("Test1");
        account.setAmount(1000);
        Account account2 = new Account();
        account2.setAccountName("Test2");
        account2.setAmount(1000);
        transferService.getAccountService().create(account);
        transferService.getAccountService().create(account2);
    }


    @Test
    public void successfulTransferTest() {
        AccountService accountService = transferService.getAccountService();
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setDestinationAccountNumber("1001");
        transferRequest.setSourceAccountNumber("1002");
        transferRequest.setAmount(10);

        double amt = accountService.get("1002").getAmount();
        double destAmt = accountService.get("1001").getAmount();
        TransferResponse transferResponse = transferService.transfer(transferRequest);

        Assert.assertEquals(TransferUtil.SUCCESS_MESSAGE,transferResponse.getMessage());
        Assert.assertEquals(TransferUtil.SUCCESS_CODE,transferResponse.getCode());
        Assert.assertNotNull(transferResponse.getReference());
        Assert.assertEquals(amt - 10, accountService.get("1002").getAmount(),0.0);
        Assert.assertEquals(destAmt + 10, accountService.get("1001").getAmount(),0.0);
    }

    @Test
    public void insufficientFundsTransferTest() {
        AccountService accountService = transferService.getAccountService();
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setDestinationAccountNumber("1001");
        transferRequest.setSourceAccountNumber("1002");
        transferRequest.setAmount(2000);

        double amt = accountService.get("1002").getAmount();
        double destAmt = accountService.get("1001").getAmount();
        TransferResponse transferResponse = transferService.transfer(transferRequest);

        Assert.assertEquals(TransferUtil.INSUFFICIENT_FUNDS_MESSAGE,transferResponse.getMessage());
        Assert.assertEquals(TransferUtil.FAIL_CODE,transferResponse.getCode());
        Assert.assertNotNull(transferResponse.getReference());
        Assert.assertEquals(amt, accountService.get("1002").getAmount(),0.0);
        Assert.assertEquals(destAmt, accountService.get("1001").getAmount(),0.0);
    }

    @Test
    public void invalidSourceAccountNumberTransferTest() {
        AccountService accountService = transferService.getAccountService();
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setDestinationAccountNumber("1001");
        transferRequest.setSourceAccountNumber("1009");
        transferRequest.setAmount(2000);

        double amt = accountService.get("1002").getAmount();
        double destAmt = accountService.get("1001").getAmount();
        TransferResponse transferResponse = transferService.transfer(transferRequest);

        Assert.assertEquals(TransferUtil.INVALID_ACCOUNT,transferResponse.getMessage());
        Assert.assertEquals(TransferUtil.FAIL_CODE,transferResponse.getCode());
        Assert.assertNotNull(transferResponse.getReference());
        Assert.assertEquals(amt, accountService.get("1002").getAmount(),0.0);
        Assert.assertEquals(destAmt, accountService.get("1001").getAmount(),0.0);
    }

    @Test
    public void invalidDestinationAccountNumberTransferTest() {
        AccountService accountService = transferService.getAccountService();
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setDestinationAccountNumber("1009");
        transferRequest.setSourceAccountNumber("1002");
        transferRequest.setAmount(1000);

        double amt = accountService.get("1002").getAmount();
        double destAmt = accountService.get("1001").getAmount();
        TransferResponse transferResponse = transferService.transfer(transferRequest);

        Assert.assertEquals(TransferUtil.INVALID_ACCOUNT,transferResponse.getMessage());
        Assert.assertEquals(TransferUtil.FAIL_CODE,transferResponse.getCode());
        Assert.assertNotNull(transferResponse.getReference());
        Assert.assertEquals(amt, accountService.get("1002").getAmount(),0.0);
        Assert.assertEquals(destAmt, accountService.get("1001").getAmount(),0.0);
    }

}
