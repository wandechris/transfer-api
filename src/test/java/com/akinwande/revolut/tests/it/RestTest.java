package com.akinwande.revolut.tests.it;

import com.akinwande.revolut.TransferApplication;
import io.undertow.Handlers;
import io.undertow.Undertow;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class RestTest {
    static String ACCOUNT_URL = "http://localhost:8080/v1/accounts";
    static String TRANSFER_URL = "http://localhost:8080/v1/transfers";
    @BeforeClass
    public static void beforeClass() {
        UndertowJaxrsServer server = new UndertowJaxrsServer().start(Undertow.builder().addHttpListener(8080,
                "localhost").setHandler(Handlers.gracefulShutdown(Handlers.path())));
        server.deploy(TransferApplication.class, "/");
    }


    @Test
    public void accountCreationTest() throws Exception {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(ACCOUNT_URL);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount", 1000);
        jsonObject.put("accountName", "Test1");

        String json = jsonObject.toString();
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = client.execute(httpPost);
        client.close();
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
    }

    public void createAccount(int number) throws Exception{
        CloseableHttpClient client = HttpClients.createDefault();
        for(int i=1;i<=number;i++) {
            HttpPost httpPost = new HttpPost(ACCOUNT_URL);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount", 1000);
            jsonObject.put("accountName", "Test"+i);
            String json = jsonObject.toString();
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            client.execute(httpPost);
        }
        client.close();
    }

    @Test
    public void doSuccessfulTransferTest() throws Exception {
        createAccount(2);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(TRANSFER_URL);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",1000);
        jsonObject.put("sourceAccountNumber","1001");
        jsonObject.put("destinationAccountNumber","1002");

        String json = jsonObject.toString();
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        client.close();
    }


    @Test
    public void doInsufficientFundsTransferTest() throws Exception {
        createAccount(2);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(TRANSFER_URL);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",5000);
        jsonObject.put("sourceAccountNumber","1001");
        jsonObject.put("destinationAccountNumber","1002");

        String json = jsonObject.toString();
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 400);
        client.close();
    }

    @Test
    public void doInvalidAccountTest() throws Exception {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(TRANSFER_URL);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",5000);
        jsonObject.put("sourceAccountNumber","1001");
        jsonObject.put("destinationAccountNumber","1002");

        String json = jsonObject.toString();
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 400);
        client.close();
    }
}
