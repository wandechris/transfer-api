package com.akinwande.revolut;

import io.undertow.Handlers;
import io.undertow.Undertow;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;

public class TransferServer {
    public static void main(String[] args) {
        UndertowJaxrsServer server = new UndertowJaxrsServer().start(buildServer());
        server.deploy(TransferApplication.class, "/");
    }

    protected static Undertow.Builder buildServer() {
        return Undertow.builder().addHttpListener(8080,
                "localhost").setHandler(Handlers.gracefulShutdown(Handlers.path()));
    }
}

