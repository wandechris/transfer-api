package com.akinwande.revolut.util;

public class TransferUtil {
    public static String SUCCESS_CODE = "00";
    public static String SUCCESS_MESSAGE = "Successful";
    public static String FAIL_CODE = "99";
    public static String FAIL_MESSAGE = "Failed";
    public static String INCOMPLETE_CODE = "01";
    public static String INCOMPLETE_MESSAGE = "Incomplete";
    public static String INSUFFICIENT_FUNDS_MESSAGE = "Insufficient Funds";
    public static String INVALID_ACCOUNT = "Invalid Account";
    public static String INVALID_AMOUNT = "Invalid Amount";
    public static String INVALID_DESTINATION_ACCOUNT_NUMBER = "Invalid Destination Account Number";
    public static String INVALID_SOURCE_ACCOUNT_NUMBER = "Invalid Source Account Number";
    public static String INVALID_ACCOUNT_NAME = "Invalid Account Name";


    public static String getMessage(String code){
        switch(code) {
            case "00" :
               return SUCCESS_MESSAGE;
            case "01" :
                return INCOMPLETE_MESSAGE;
            default :
                return FAIL_MESSAGE;
        }
    }
}
