package com.akinwande.revolut;

import com.akinwande.revolut.controller.AccountController;
import com.akinwande.revolut.controller.TransferController;
import com.akinwande.revolut.repository.AccountRepository;
import com.akinwande.revolut.repository.Impl.AccountRepositoryMapImpl;
import com.akinwande.revolut.repository.TransfersRepository;
import com.akinwande.revolut.repository.Impl.TransfersRepositoryMapImpl;
import com.akinwande.revolut.service.AccountService;
import com.akinwande.revolut.service.TransferService;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

public class TransferModule implements Module {
    public void configure(final Binder binder) {
        binder.bind(TransferController.class);
        binder.bind(AccountController.class);
        binder.bind(TransferService.class).in(Singleton.class);
        binder.bind(AccountService.class).in(Singleton.class);
        binder.bind(TransfersRepository.class).to(TransfersRepositoryMapImpl.class).in(Singleton.class);
        binder.bind(AccountRepository.class).to(AccountRepositoryMapImpl.class).in(Singleton.class);
    }
}
