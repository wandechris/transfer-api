package com.akinwande.revolut.controller;

import com.akinwande.revolut.dao.Account;
import com.akinwande.revolut.dao.Error;
import com.akinwande.revolut.service.AccountService;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1")
public class AccountController {

    @Inject
    public AccountService accountService;

    @POST
    @Path("/accounts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount(Account account){
        if(account == null){
            return Response.ok(new Error(Response.Status.BAD_REQUEST.getStatusCode(),"Request cannot be null")).status(Response.Status.BAD_REQUEST).build();
        }
        try {
            validate(account);
            Account retAccount = accountService.create(account);
            return Response.ok(retAccount).status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.ok(new Error(Response.Status.BAD_REQUEST.getStatusCode(),ex.getMessage())).status(Response.Status.BAD_REQUEST).build();
        }
    }

    public void validate(Account account) throws Exception{
        if(account.getAmount() <= 0){
            throw new Exception(TransferUtil.INVALID_AMOUNT);
        }
        if(account.getAccountName() == null || account.getAccountName().isEmpty()){
            throw new Exception(TransferUtil.INVALID_ACCOUNT_NAME);
        }
    }
}
