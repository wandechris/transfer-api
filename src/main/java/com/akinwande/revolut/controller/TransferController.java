package com.akinwande.revolut.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.akinwande.revolut.dao.Error;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.dao.TransferResponse;
import com.akinwande.revolut.service.TransferService;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;

@Path("/v1")
public class TransferController {

    @Inject
    TransferService transferService;

    @POST
    @Path("/transfers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doTransfer(TransferRequest transferRequest){
        if(transferRequest == null){
            return Response.ok(new Error(Response.Status.BAD_REQUEST.getStatusCode(),"Request cannot be null")).status(Response.Status.BAD_REQUEST).build();
        }
        try {
            validate(transferRequest);
            TransferResponse transferResponse = transferService.transfer(transferRequest);
            if(transferResponse.getCode().equalsIgnoreCase("00")){
                return Response.ok(transferResponse).status(Response.Status.OK).build();
            }else{
                return Response.ok(transferResponse).status(Response.Status.BAD_REQUEST).build();
            }

        }catch (Exception ex){
            return Response.ok(new Error(Response.Status.BAD_REQUEST.getStatusCode(),ex.getMessage())).status(Response.Status.BAD_REQUEST).build();
        }
    }

    public void validate(TransferRequest transferRequest) throws Exception{
        if(transferRequest.getAmount() <= 0){
            throw new Exception(TransferUtil.INVALID_AMOUNT);
        }

        if(transferRequest.getDestinationAccountNumber() == null || transferRequest.getDestinationAccountNumber().isEmpty()){
            throw new Exception(TransferUtil.INVALID_DESTINATION_ACCOUNT_NUMBER);
        }

        if(transferRequest.getSourceAccountNumber() == null || transferRequest.getSourceAccountNumber().isEmpty()){
            throw new Exception(TransferUtil.INVALID_SOURCE_ACCOUNT_NUMBER);
        }
    }
}
