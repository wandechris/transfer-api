package com.akinwande.revolut.service;

import com.akinwande.revolut.dao.Transfer;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.dao.TransferResponse;
import com.akinwande.revolut.repository.TransfersRepository;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;


public class TransferService {
    @Inject
    public AccountService accountService;
    @Inject
    public TransfersRepository transfersRepository;

//    public TransferService

    public synchronized TransferResponse transfer(TransferRequest transferRequest){
        TransferResponse transferResponse = new TransferResponse();
        //Generate Unique reference
        if(transferRequest.getReference() == null)  transferRequest.setReference(createHash(transferRequest));
        try {
            if(!transfersRepository.exists(transferRequest.getReference())) {
                accountService.debit(transferRequest.getSourceAccountNumber(), transferRequest.getAmount());
                accountService.credit(transferRequest.getSourceAccountNumber(), transferRequest.getDestinationAccountNumber(), transferRequest.getAmount());
                //save transfer entry
                save(transferRequest, TransferUtil.SUCCESS_CODE);
                transferResponse.setMessage(TransferUtil.getMessage(TransferUtil.SUCCESS_CODE));
                transferResponse.setCode(TransferUtil.SUCCESS_CODE);
            }else{
                Transfer transfer = transfersRepository.get(transferRequest.getReference());
                transferResponse.setCode(transfer.getStatus());
                transferResponse.setMessage(TransferUtil.getMessage(transfer.getStatus()));
            }
        }catch (Exception ex){
            if(transfersRepository.get(transferRequest.getReference()) == null){
                save(transferRequest, TransferUtil.FAIL_CODE);
            }
            transferResponse.setMessage(ex.getMessage());
            transferResponse.setCode(TransferUtil.FAIL_CODE);
        }
        transferResponse.setReference(transferRequest.getReference());
        return transferResponse;
    }

    private String createHash(TransferRequest transferRequest){
        String concat = transferRequest.getSourceAccountNumber()+transferRequest.getDestinationAccountNumber()+transferRequest.getAmount()+System.currentTimeMillis();
        return String.valueOf(concat.hashCode() & 0xfffffff);
    }

    public AccountService getAccountService() {
        return accountService;
    }

    public void save(TransferRequest transferRequest, String status){
        transfersRepository.save(transferRequest,status);
    }
}