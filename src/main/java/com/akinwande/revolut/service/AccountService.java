package com.akinwande.revolut.service;

import com.akinwande.revolut.dao.Account;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.repository.AccountRepository;
import com.akinwande.revolut.util.TransferUtil;
import com.google.inject.Inject;

public class AccountService {
    @Inject
    public AccountRepository accountRepository;
    @Inject
    TransferService transferService;

    public double credit(String sourceAccountNumber,String destinationAccountNumber, double amount) throws Exception{
        if(get(destinationAccountNumber) != null){
            return accountRepository.credit(destinationAccountNumber,amount);
        }else{
            if(sourceAccountNumber != null) {
                //reversal
                credit(null, sourceAccountNumber, amount);
            }else {
                TransferRequest transferRequest = new TransferRequest();
                transferRequest.setDestinationAccountNumber(destinationAccountNumber);
                transferRequest.setAmount(amount);
                transferService.save(transferRequest,TransferUtil.INCOMPLETE_CODE);

            }
            throw new Exception(TransferUtil.INVALID_ACCOUNT);
        }
    }

    public double debit(String accountNumber, double amount) throws Exception{
        return accountRepository.debit(accountNumber,amount);
    }

    public Account get(String accountNumber) {
        return accountRepository.get(accountNumber);
    }

    public synchronized Account create(Account account){
        String accountNumber  = "100"+(accountRepository.countAll()+1);
        account.setAccountNumber(accountNumber);
        accountRepository.create(account);
        return account;
    }
}
