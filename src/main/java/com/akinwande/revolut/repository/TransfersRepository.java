package com.akinwande.revolut.repository;


import com.akinwande.revolut.dao.Transfer;
import com.akinwande.revolut.dao.TransferRequest;

public interface TransfersRepository {
    void save(TransferRequest transferRequest, String status);
    Transfer get(String reference);
    boolean exists(String reference);
}
