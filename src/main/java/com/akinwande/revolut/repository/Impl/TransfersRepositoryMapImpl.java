package com.akinwande.revolut.repository.Impl;

import com.akinwande.revolut.dao.Transfer;
import com.akinwande.revolut.dao.TransferRequest;
import com.akinwande.revolut.repository.TransfersRepository;

import java.util.concurrent.ConcurrentHashMap;

public class TransfersRepositoryMapImpl implements TransfersRepository {
    private ConcurrentHashMap<String, Transfer> transfers;

    public TransfersRepositoryMapImpl(){
        transfers = new ConcurrentHashMap<>();
    }

    @Override
    public void save(TransferRequest transferRequest, String status) {
        transfers.put(transferRequest.getReference(), new Transfer(transferRequest.getSourceAccountNumber(), transferRequest.getDestinationAccountNumber(), transferRequest.getAmount(),
                transferRequest.getReference() , status));
    }

    @Override
    public Transfer get(String reference) {
        return transfers.get(reference);
    }

    @Override
    public boolean exists(String reference) {
        return transfers.keySet().contains(reference);
    }
}
