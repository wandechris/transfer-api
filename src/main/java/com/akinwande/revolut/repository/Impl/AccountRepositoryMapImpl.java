package com.akinwande.revolut.repository.Impl;

import com.akinwande.revolut.dao.Account;
import com.akinwande.revolut.repository.AccountRepository;
import com.akinwande.revolut.util.TransferUtil;
import java.util.concurrent.ConcurrentHashMap;

public class AccountRepositoryMapImpl implements AccountRepository {
    private ConcurrentHashMap<String, Account> accounts;

    public AccountRepositoryMapImpl(){
        accounts = new ConcurrentHashMap<>();
//        accounts.put("1001",new Account("Test1",1000,"1001"));
//        accounts.put("1002",new Account("Test2",1000,"1002"));
    }

    @Override
    public double credit(String accountNumber, double amount){
        Account account = accounts.get(accountNumber);
        double newAmount = account.getAmount() + amount;
        account.setAmount(newAmount);
        accounts.put(accountNumber,account);
        return newAmount;
    }

    @Override
    public double debit(String accountNumber, double amount) throws Exception{
        if(get(accountNumber) != null){
            Account account = accounts.get(accountNumber);
            if(account.getAmount() < amount){
                throw new Exception(TransferUtil.INSUFFICIENT_FUNDS_MESSAGE);
            }
            double newAmount = account.getAmount() - amount;
            account.setAmount(newAmount);
            accounts.put(accountNumber,account);
            return newAmount;
        }else{
            throw new Exception(TransferUtil.INVALID_ACCOUNT);
        }
    }

    @Override
    public Account get(String accountNumber) {
        return accounts.get(accountNumber);
    }

    @Override
    public Account create(Account account) {
        return accounts.put(account.getAccountNumber(),account);
    }

    @Override
    public int countAll() {
        return accounts.keySet().size();
    }
}
