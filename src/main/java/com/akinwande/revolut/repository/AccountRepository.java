package com.akinwande.revolut.repository;

import com.akinwande.revolut.dao.Account;

public interface AccountRepository {
    double credit (String accountNumber, double amount) throws Exception;
    double debit (String accountNumber, double amount) throws Exception;
    Account get(String accountNumber);
    int countAll();
    Account create(Account account);
}
