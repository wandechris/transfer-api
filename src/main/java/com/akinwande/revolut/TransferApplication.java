package com.akinwande.revolut;

import com.akinwande.revolut.controller.AccountController;
import com.akinwande.revolut.controller.TransferController;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TransferApplication extends Application {
    private Set<Object> singletons = new HashSet<>();
    private Set<Class<?>> classes = new HashSet<>();

    public TransferApplication() {
        Injector injector = Guice.createInjector(new TransferModule());
        TransferController transferController = injector.getInstance(TransferController.class);
        AccountController accountController = injector.getInstance(AccountController.class);
        singletons.add(transferController);
        singletons.add(accountController);
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Map<String, Object> getProperties() {
        return super.getProperties();
    }

}
