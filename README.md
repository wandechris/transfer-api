## How to run:
1. Run `mvn clean package`
2. Run ` java -jar target/transfer-api.jar `
4. Server will run on  `http://localhost:8080/` 
5. run `curl -X POST \
         http://localhost:8080/v1/accounts \
         -H 'Content-Type: application/json' \
         -H 'Postman-Token: 67e22522-0576-469e-a9c9-547c00db5a6c' \
         -H 'cache-control: no-cache' \
         -d '{
          "accountName":"Test1",
           "amount":70
       }'` to create a test account
6. run `curl -X POST \
         http://localhost:8080/v1/transfers \
         -H 'Content-Type: application/json' \
         -H 'Postman-Token: 5de717e9-a140-41f9-a729-9fa2e04527e8' \
         -H 'cache-control: no-cache' \
         -d '{
           "sourceAccountNumber": "1001",
           "destinationAccountNumber": "1002",
           "amount": 500
       }'` to initiate a transfer
       
7. import Transfer.postman_collection.json to postman to try